<?php

namespace EGOL\SEPA;

use EGOL\SEPA\Exceptions\SessionInitException;
use EGOL\SEPA\Exceptions\NoConnectionException;

class SEPA 
{
    static $errors = array(
        1 =>  'ERROR_UNKNOWN (unbekannte Prüfziffermethode – sollte eigentlich nie auftreten, außer evtl. bei Kreditkartennummern, bei denen der Kreditkartentyp nicht identifiziert werden kann)',
        2 =>  'ERROR_NOT_IMPLEMENTED (nicht implementierte Prüfziffermethode – sollte eigentlich auch nie auftreten)',
        3 =>  'ERROR_NO_PRUEFZIFFER (diese Bank verwendet generell keine Prüfziffern)',
        4 =>  'ERROR_SPECIAL (dies ist eine Kontonummer aus einem speziellen Kontonummernbereich dieser Bank, innerhalb desen keine Prüfung möglich ist)',
        5 =>  'ERROR_BLZ_ONLY (es wurde nur die Bankleitzahl, nicht jedoch die Kontonummer geprüft; z.B. weil für dieses Land keine Kontonummernprüfung möglich ist)',
        6 =>  'ERROR_IBAN_COUNTRY_NOT_BOOKED (die IBAN-Prüfziffer ist in Ordnung; eine weitergehende Prüfung wurde nicht durchgeführt, weil Sie das betreffende Land nicht gebucht haben)',
        7 =>  'ERROR_IBAN_COUNTRY_NOT_AVAILABLE (die IBAN-Prüfziffer ist in Ordnung; eine weitergehende Prüfung wurde nicht durchgeführt, weil das betreffende Land nicht geprüft werden kann)',
        12 =>  'ERROR_KTO_NO_DIGITS (Kontonummer enthält nicht nur Ziffern)',
        13 =>  'ERROR_KTO_PRUEFZIFFER (Kontonummer falsch, Prüfziffer paßt nicht)',
        14 =>  'ERROR_KTO_ZERO (Kontonummer ist 0 und damit unzulässig)',
        15 =>  'ERROR_KTO_TYPE (Kontoartenziffer ist ungültig)',
        16 =>  'ERROR_KTO_LEN8 (Kontonummer muß genau acht Stellen lang sein)',
        17 =>  'ERROR_KTO_LEN9 (Kontonummer muß genau neun Stellen lang sein)',
        18 =>  'ERROR_KTO_LEN7 (Kontonummer muß genau sieben Stellen lang sein)',
        19 =>  'ERROR_KTO_LEN8OR10 (Kontonummer muß acht oder zehn Stellen lang sein)',
        20 =>  'ERROR_KTO_LEN10 (Kontonummer muß genau zehn Stellen lang sein)',
        21 =>  'ERROR_KTO_LEN7OR8 (Kontonummer muß sieben oder acht Stellen lang sein)',
        22 =>  'ERROR_KTO_IBAN_PRUEFZIFFER (IBAN-Prüfziffer falsch; weitergehende Prüfungen wurden daher nicht durchgeführt)',
        23 =>  'ERROR_KTO_BLACKLIST (diese Bankverbindung steht auf Ihrer Blacklist)',
        24 =>  'ERROR_GET_IBAN (die IBAN kann nicht ermittelt werden; kann nur bei der Funktion KtoPruef.GetIban von KONTOPRUEF-OFFLINE auftreten; für nähere Angaben siehe dort)',
        25 =>  'ERROR_KTO_IBAN_COUNTRY (die angegebene IBAN konnte keinem Land zugeordnet werden)',
        26 =>  'ERROR_KTO_IBAN_FORMAT (das Format der angegebenen IBAN passt nicht zum Land)',
        -100 => 'ERROR_USER_OR_PASSWORD (Benutzername und/oder Passwort sind ungültig oder abgelaufen)',
        -200 => 'ERROR_NO_CONNECTION (Verbindung zum Server kann nicht hergestellt werden - tritt nur beim COM-Objekt HanftWddx auf; die Property Resulttext enthält in diesem Fall nähere Angaben)',
        -250 => 'ERROR_CERTIFICATE (Das Serverzertifikat von wddx.hanft.de kann nicht geprüft werden. Den genauen Grund finden Sie im Antwortfeld resulttext. In der Regel liegt dieses Problem daran, daß in Ihrer Windows-Version das Zertifikat des Unterzeichners noch nicht installiert ist. Führen Sie ein Windows-Update durch und kreuzen Sie dort insbesondere das "Stammzertifikatupdate" an (möglicherweise wird es Ihnen nicht bei den "wichtigen", sondern bei den "optionalen" Updates angeboten). Falls Sie Ihre Windows-Version nicht updaten wollen, können Sie auch SSLMode=2 setzen; dann wird immer noch verschlüsselt übertragen, es wird jedoch nicht geprüft, ob es sich beim Server wirklich um wddx.hanft.de handelt).',
        -300 => 'ERROR_NON_NUMERICAL_RESULT (der Server hat ein nicht-numerisches Ergebnis im Feld Result zurückgeliefert – sollte eigentlich nie vorkommen)'
    );

    protected $url = "http://wddx.hanft.de/ktopruef";
    /**
     * BIC String
     * @var string
     */
    private $bic;
    /**
     * IBAN String
     * @var string
     */
    private $iban;
    /**
     * Ergebnis aus der Prüfung
     * @var array
     */
    private $curl_result;
    /**
     * Fehlercode aus dem Ergebnis 
     * @var int
     */
    private $result;

    /**
     * Ergebnis aus der BIC Prüfung
     * @var string
     */
    private $result_bic;

    /**
     * Hanft Benutzername
     * @var string
     */
    private $user;

    /**
     * Hanft Passwort
     * @var string
     */
    private $password;

    /**
     * Konstruktor erwartet IBAN und BIC
     * @param string $iban IBAN Zeichenkette
     * @param string $bic  BIC Zeichenkette
     */
    public function __construct ($user, $password, $iban) {
        $this->user = $user;
        $this->password = $password;
        $this->iban = preg_replace('/\s+/', '', $iban);
    }
    /**
     * Startet die Überprüfung bei KTOPRUEF
     * http://www.kontopruef.de/online-ktopruef.shtml
     */
    public function start () {
        $ch = curl_init ($this->url . '?un=' . $this->user. "&pw=" . $this->password . "&iban=" . $this->iban);
        if(!$ch)
            throw new SessionInitException;

        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_NOBODY, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        $curl_exec_return = curl_exec($ch);

        if(!$curl_exec_return)
            throw new NoConnectionException;

        $this->curl_result = wddx_deserialize ($curl_exec_return);
        $this->result = $this->curl_result['result'];
        $this->result_bic = $this->curl_result['bic'];

        return true;
    }
    /**
     * Gibt den Schlüssel "result" aus dem Ergebnis der Prüfung wieder.
     * Sollte die Prüfung >= 0 und <= 10 sein, müssen die Eingaben akzepziert 
     * werden.
     * @return int 
     */
    public function result() {
        return $this->result;
    }
    /**
     * Debug Methode.
     * Gibt das Ergebnis komplett in lesbarer Form wieder. Nicht für den Live-
     * Betrieb geeignet!
     * @return array 
     */
    public function print_curl_result() {
        return $this->curl_result;
    }
    public function get_result_bic() {
        return $this->result_bic;
    }
}