<?php

namespace EGOL\SEPA\Exceptions;

/**
* Session Init Exception
*/
class SessionInitException extends Exception
{
    
}