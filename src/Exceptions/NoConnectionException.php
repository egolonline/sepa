<?php

namespace EGOL\SEPA\Exceptions;

/**
* No Connection Exception
*/
class NoConnectionException extends Exception
{

}