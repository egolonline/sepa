<?php

use EGOL\SEPA\SEPA;

class SepaTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        /**
         * Benutzername und Passwort bitte eintragen
         * @var SEPA
         */
        $this->sepa = new SEPA('', '', '');
    }

    /**
     * @test
     */
    public function canValidateIban() 
    {
        $x = $this->sepa->start();
        $this->assertEquals(0, $this->sepa->result());
    }
}
